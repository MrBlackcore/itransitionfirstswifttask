import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var loginErrorLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    let animationService = AnimationService()
    
    //MARK: - Main Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - IBActions

    @IBAction func loginButtonIsPressed(_ sender: UIButton) {
        checkTextField(loginTextField, labelForErrors: loginErrorLabel)
        checkTextField(passwordTextField, labelForErrors: passwordErrorLabel)
    }
    
    //MARK: - Flow Functions
    
    func checkTextField(_ textField: UITextField, labelForErrors label: UILabel) {
        guard let text = textField.text, !text.isEmpty else {
            renewLabel(label, with: "Field is empty")
            return
        }
        checkRegexFor(text, in: textField, label: label)
    }
    
    func checkRegexFor(_ text: String,in textField: UITextField, label: UILabel) {
        switch textField {
        case loginTextField:
            if !text.isValidEmail {
                renewLabel(label, with: "Email is invalid")
            } else {
                renewLabel(label, with: "")
            }
        case passwordTextField:
            if !text.isValidPassword {
                renewLabel(label, with: "Password is invalid")
            } else {
                renewLabel(label, with: "")
            }
        default:
            break
        }
    }
    
    func renewLabel(_ label: UILabel, with text: String) {
        label.text = text
        checkLabel(label)
    }
    
    func checkLabel(_ label: UILabel) {
        if label.isHidden {
            animationService.animateLabelTransition(label: label)
        }
        if label.text == "" && !label.isHidden {
            animationService.animateLabelTransition(label: label)
        }
    }
    
}
