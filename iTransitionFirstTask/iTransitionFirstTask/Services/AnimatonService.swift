import UIKit

class AnimationService {
    
    func animateLabelTransition(label: UILabel) {
        UIView.transition(with: label, duration: 0.3, options: .transitionCrossDissolve) {
            self.changeAlphaFor(label, isHidden: label.isHidden)
            label.isHidden.toggle()
        }
    }
    
    private func changeAlphaFor(_ label: UILabel, isHidden: Bool) {
        switch isHidden {
        case true:
            label.alpha = 1
        case false:
            label.alpha = 0
        }
    }

}
