import UIKit

class CustomButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureButton()
    }
    
    func configureButton() {
        backgroundColor = .orange
        layer.cornerRadius = 10
        layer.borderWidth = 2
        layer.borderColor = UIColor.red.cgColor
        tintColor = .black
    }
}
