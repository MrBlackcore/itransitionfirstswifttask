import UIKit

extension String {
    
    var isValidEmail:Bool {
        NSPredicate(format: "SELF MATCHES %@", "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}").evaluate(with: self)
    }
    
    var isValidPassword:Bool {
        NSPredicate(format: "SELF MATCHES %@", "^[a-z0-9A-Z._%+-]*$").evaluate(with: self)
    }
    
}
